# simplay-assets

All the assets for Simplay Survival. That includes:
  - Textures
  - Sounds
  - Models
  - Music
  - Logos